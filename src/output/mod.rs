pub mod simple;

pub enum OutputInstruction<'a> {
	Text(&'a str),
	NewLine,
}


pub trait Output {
    	fn text<'a>(text:&'a str) {
		Self::print( &OutputInstruction::Text(text));
    	}

	fn new_line<'a> () {
		Self::print( &OutputInstruction::NewLine);
	}
    	
	fn print<'a>(instruction: &OutputInstruction);
}
