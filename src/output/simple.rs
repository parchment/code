use crate::output::{Output, OutputInstruction};

#[derive(Debug)]
pub struct SimpleOutput {}

impl Output for SimpleOutput {
        fn print<'a>(instruction: &OutputInstruction<'a>) {
                match instruction {
                        OutputInstruction::Text(text) => print!("{}", text),
                        OutputInstruction::NewLine => println!(""),
                }
        }
}
