use crate::commands::{CommandError, InstallAction, Result, StatusAction};
use crate::config::{Config, PMWrapperDir};
use crate::model::{Host, Link, LinkStatus, PackageManager, Status};
use cmd_lib::{run_cmd, run_fun};
use directories::BaseDirs;
use im::OrdSet;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub struct PackageManagerWrapper(pub PackageManager, PathBuf);

impl PackageManager {
    pub fn wrap(&self, pm_root: &Path) -> Result<PackageManagerWrapper> {
        let &PackageManager(name) = &self;
        let mut pm_dir = pm_root.to_path_buf();
        pm_dir.push(name);
        if !pm_dir.exists() {
            return Err(CommandError::WrapperNotFound(self.to_owned(), pm_dir));
        }

        Ok(PackageManagerWrapper(self.to_owned(), pm_dir))
    }
}

impl StatusAction for PackageManagerWrapper {
    fn status(&self, packages: &OrdSet<Link>) -> Vec<LinkStatus> {
        let PackageManagerWrapper(_, pm_root) = self;
        let pstrs: Vec<String> = packages
            .into_iter()
            .map(|Link(_, p)| p.to_string())
            .collect();

        let res = run_fun! {
                    $pm_root/status $[pstrs]
        }
        .unwrap();
        res.lines()
            .flat_map(|l| l.split_once(" "))
            .map(|(package, status)| {
                LinkStatus(
                    Link(self.0.to_owned(), package.to_owned()),
                    Status::from(status),
                )
            })
            .collect()
    }

    fn load(
        pm: &PackageManager,
        pm_wrapper_dir: &PMWrapperDir<PathBuf>,
    ) -> Result<PackageManagerWrapper> {
        pm.wrap(&pm_wrapper_dir.0)
    }
}

impl InstallAction for PackageManagerWrapper {
    fn install(&self, packages: &OrdSet<Link>) -> u16 {
        let PackageManagerWrapper(_, pm_root) = self;
        let pstrs: Vec<String> = packages
            .into_iter()
            .map(|Link(_, p)| p.to_string())
            .collect();
        run_cmd! (
                $pm_root/install $[pstrs] 2>&1
        )
        .unwrap();
        4711
    }

    fn load(
        pm: &PackageManager,
        pm_wrapper_dir: &PMWrapperDir<PathBuf>,
    ) -> std::result::Result<Self, CommandError> {
        pm.wrap(&pm_wrapper_dir.0)
    }
}

pub fn setup_env(config: &Config) {
    let base_dirs = BaseDirs::new().expect("base dirs");
    let home_dir = base_dirs.home_dir();
    std::env::set_var(
        "DOTFILES",
        format!("{}/{}", home_dir.to_string_lossy(), config.dotfiles.0),
    );
    std::env::set_var("HOST", format!("{}", Host::default()));
}
