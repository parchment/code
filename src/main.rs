mod commands;
mod config;
pub mod model;
mod output;
mod package_manager_wrapper;
mod parchment;

use crate::{output::simple::SimpleOutput, parchment::vfs::VfsParchmentRepository};
use color_eyre::eyre::Result;
use commands::{info, install, ls, status, Command};
use config::{ConfigLocation, PMWrapperDir};
use directories::ProjectDirs;
use model::Host;
use structopt::StructOpt;
use tracing::debug_span;

fn setup_tracing() {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{fmt, EnvFilter};

    let fmt_layer = fmt::layer().with_target(false);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(fmt_layer)
        .with(ErrorLayer::default())
        .init();
}

fn setup_error_reporting() -> Result<()> {
    color_eyre::install()
}

#[tracing::instrument]
fn main() -> Result<()> {
    setup_tracing();
    setup_error_reporting()?;
    let command = Command::from_args();

    let _span = debug_span!("interpret command", ?command);
    let project_dirs =
        ProjectDirs::from("se", "johlrogge", "parchment").expect("could not find project dirs");

    let config_location: ConfigLocation<_> = (&project_dirs).into();
    let pm_wrapper_dir: PMWrapperDir<_> = (&project_dirs).into();
    let host = Host::default();
    let config = config_location.load()?;
    package_manager_wrapper::setup_env(&config);
    let dotfiles_repository = VfsParchmentRepository::new(config.lookup_dotfiles_root()?);
    match command {
        Command::Status {} => {
            status::<_, SimpleOutput>(dotfiles_repository, &host, &pm_wrapper_dir)?
        }
        Command::Install { dry_run, force } => {
            install::<_, SimpleOutput>(dotfiles_repository, force, dry_run, &host, &pm_wrapper_dir)?
        }
        Command::Ls {} => ls::<_, SimpleOutput>(&host, dotfiles_repository)?,
        Command::Info {} => info::info::<SimpleOutput>(&config_location, &pm_wrapper_dir)?,
    };
    Ok(())
}
