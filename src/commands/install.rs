use crate::model::{Dependency, Host, Link, LinkStatus, PackageManager, Status};
use crate::output::Output;
use crate::package_manager_wrapper::PackageManagerWrapper;
use crate::parchment::read_parchments_for_host;
use crate::parchment::ParchmentRepository;
use crate::{
    commands::{status::calculate_statuses, InstallAction},
    config::PMWrapperDir,
};
use ::im::{ordset, OrdSet};
use color_eyre::eyre::Result;
use core::fmt::Debug;
use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::path::PathBuf;
use tracing::event;

#[tracing::instrument]
pub fn install<'a, PM, O>(
    dotfiles_root: PM,
    force: bool,
    dry_run: bool,
    host: &Host,
    pm_wrapper_dir: &PMWrapperDir<PathBuf>,
) -> Result<&'a str>
where
    PM: ParchmentRepository + Debug,
    O: Output,
{
    let (host_parchments, dependencies) = read_parchments_for_host(&host, &dotfiles_root)?;
    let statuses = if force {
        host_parchments
            .iter()
            .map(|link| LinkStatus(link.clone(), Status::NotInstalled))
            .collect()
    } else {
        calculate_statuses::<PackageManagerWrapper>(host_parchments, pm_wrapper_dir)?
    };
    let not_installed_links: OrdSet<Link> = statuses
        .iter()
        .cloned()
        .filter(|LinkStatus(_link, status)| status == &Status::NotInstalled)
        .map(|LinkStatus(link, _)| link)
        .collect();
    let packages_by_pm = order_install_by_dependencies(not_installed_links, dependencies);

    packages_by_pm
        .iter()
        .for_each(|(package_manager, packages)| {
            let pm = PackageManagerWrapper::load(&package_manager, &pm_wrapper_dir)
                .expect("could not load package manager wrapper");
            O::text(&format!(
                "{}:
    {}",
                package_manager.0,
                packages
                    .iter()
                    .map(|Link(_, target)| target)
                    .cloned()
                    .collect::<Vec<String>>()
                    .join(", ")
            ));
            O::new_line();
            if !dry_run {
                pm.install(packages);
            } else {
                0;
            }
        });
    Ok("install")
}

fn depends_on(left: &Link, right: &Link, dependencies: &OrdSet<&Dependency>) -> bool {
    let left_candidates: OrdSet<&Dependency> = dependencies
        .iter()
        .cloned()
        .filter(|Dependency { to, .. }| &to == &right)
        .collect();
    let right_candidates: OrdSet<&Dependency> = dependencies
        .iter()
        .cloned()
        .filter(|Dependency { from, .. }| &from == &left)
        .collect();
    let candidates = (left_candidates.is_empty(), right_candidates.is_empty());
    match candidates {
        (false, false) => {
            let intersection = left_candidates.clone().intersection(right_candidates);
            (!intersection.is_empty())
                || left_candidates
                    .iter()
                    .find(|Dependency { from, .. }| depends_on(from, &right, &dependencies))
                    .is_some()
        }
        _ => false,
    }
}

fn ord_by_dependency(left: &Link, right: &Link, dependencies: &OrdSet<&Dependency>) -> Ordering {
    let dependency_status = (
        depends_on(left, right, dependencies),
        depends_on(right, left, dependencies),
    );
    match dependency_status {
        (true, false) => Ordering::Greater,
        (false, true) => Ordering::Less,
        _ => Ordering::Equal,
    }
}

fn order_install_by_dependencies(
    links: OrdSet<Link>,
    dependencies: OrdSet<Dependency>,
) -> Vec<(PackageManager, OrdSet<Link>)> {
    fn reorder_links(
        mut links: Vec<&Link>,
        dependencies: OrdSet<&Dependency>,
    ) -> Vec<(PackageManager, BTreeSet<Link>)> {
        links.sort_unstable_by(|l, r| ord_by_dependency(l, r, &dependencies));

        links.iter().fold(vec![], |mut acc, Link(pm, target)| {
            let last = acc.last_mut();
            match last {
                Some(&mut (ref last_pm, ref mut last_links)) if last_pm == pm => {
                    event!(tracing::Level::TRACE, "{} == {}", last_pm.0, pm.0);
                    last_links.insert(Link::package_link(pm.0.clone(), target));
                    ()
                }
                _ => {
                    event!(tracing::Level::TRACE, "{} != last_pm", pm.0);
                    let mut links = BTreeSet::new();
                    links.insert(Link::package_link(pm.0.clone(), target));
                    acc.push((pm.clone(), links));
                    ()
                }
            }
            acc
        })
    }
    reorder_links(links.iter().collect(), dependencies.iter().collect())
        .iter()
        .map(|(pm, links)| (pm.to_owned(), ordset![] + links.into()))
        .collect()
}

#[cfg(test)]
mod test {

    use crate::install::ord_by_dependency;
    use crate::install::{depends_on, order_install_by_dependencies};
    use crate::model::Link;
    use crate::model::PackageManager;
    use im::ordset;
    use std::cmp::Ordering;
    use tracing_test::traced_test;

    #[traced_test]
    #[test]
    fn depends_on_is_false_when_no_deps() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");

        assert!(!depends_on(&link1, &link2, &ordset![]));
    }

    #[traced_test]
    #[test]
    fn depends_on_is_true_when_left_depends_on_right() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        let dependency = link1.clone().depends_on(link2.clone());
        assert!(depends_on(&link1, &link2, &ordset![&dependency]));
    }
    #[traced_test]
    #[test]
    fn depends_on_is_true_when_left_depends_on_right_in_two_steps() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        let link3 = Link::package_link("pm", "link3");
        let dependency1 = link1.clone().depends_on(link2.clone());
        let dependency2 = link2.clone().depends_on(link3.clone());
        assert!(depends_on(
            &link1,
            &link3,
            &ordset![&dependency1, &dependency2]
        ));
    }

    #[traced_test]
    #[test]
    fn depends_on_is_true_when_left_depends_on_right_in_three_steps() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        let link3 = Link::package_link("pm", "link3");
        let link4 = Link::package_link("pm", "link4");
        let dependency1 = link1.clone().depends_on(link2.clone());
        let dependency2 = link2.clone().depends_on(link3.clone());
        let dependency3 = link3.clone().depends_on(link4.clone());
        assert!(depends_on(
            &link1,
            &link4,
            &ordset![&dependency1, &dependency2, &dependency3]
        ));
    }

    #[traced_test]
    #[test]
    fn ord_by_dependency_no_deps_are_equal() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        assert_eq!(
            ord_by_dependency(&link1, &link2, &ordset![]),
            Ordering::Equal
        );
    }
    #[traced_test]
    #[test]
    fn ord_by_dependency_depender_gt_dependency() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        let dependency = link1.clone().depends_on(link2.clone());
        assert_eq!(
            ord_by_dependency(&link1, &link2, &ordset![&dependency]),
            Ordering::Greater
        );
    }
    #[traced_test]
    #[test]
    fn ord_by_dependency_dependency_lt_dependender() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        let dependency = link1.clone().depends_on(link2.clone());
        assert_eq!(
            ord_by_dependency(&link2, &link1, &ordset![&dependency]),
            Ordering::Less
        );
    }
    #[traced_test]
    #[test]
    fn order_install_by_dependencies_two_links_no_deps_in_alphabetical_order() {
        let link1 = Link::package_link("pm", "link1");
        let link2 = Link::package_link("pm", "link2");
        let res = order_install_by_dependencies(ordset![link1.clone(), link2.clone()], ordset![]);
        assert_eq!(
            res,
            vec![(PackageManager("pm".to_owned()), ordset![link1, link2])]
        );
    }

    #[traced_test]
    #[test]
    fn order_install_by_dependencies_two_links_with_deps_in_dependency_order() {
        let link1 = Link::package_link("pm1", "link1");
        let link2 = Link::package_link("pm2", "link2");
        let res = order_install_by_dependencies(
            ordset![link1.clone(), link2.clone()],
            ordset![link1.clone().depends_on(link2.clone())],
        );
        assert_eq!(
            res,
            vec![
                (PackageManager("pm2".to_owned()), ordset![link2]),
                (PackageManager("pm1".to_owned()), ordset![link1])
            ]
        );
    }

    #[traced_test]
    #[test]
    fn order_install_by_dependencies_three_links_with_deps_in_dependency_order() {
        let link1 = Link::package_link("pm1", "link1");
        let link2 = Link::package_link("pm2", "link2");
        let link3 = Link::package_link("pm3", "link3");
        let res = order_install_by_dependencies(
            ordset![link1.clone(), link2.clone(), link3.clone()],
            ordset![
                link1.clone().depends_on(link2.clone()),
                link2.clone().depends_on(link3.clone())
            ],
        );
        assert_eq!(
            res,
            vec![
                (PackageManager("pm3".to_owned()), ordset![link3]),
                (PackageManager("pm2".to_owned()), ordset![link2]),
                (PackageManager("pm1".to_owned()), ordset![link1])
            ]
        );
    }
}
