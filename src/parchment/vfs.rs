use super::ParchmentRepository;
use crate::model::Link;
use crate::parchment::{ordset, OrdSet, PackageManager, ParchmentError};
use ::vfs::VfsPath;
use tracing::{event, Level};

#[derive(Debug)]
pub struct VfsParchmentRepository(pub(crate) vfs::VfsPath);

type ParchmentState = (VfsPath, Option<VfsPath>);
impl VfsParchmentRepository {
    pub fn new(root: VfsPath) -> Self {
        VfsParchmentRepository(root)
    }

    fn check_parchment_state(&self, link: &Link) -> super::Result<ParchmentState> {
        let Self(root) = self;
        let parchment_base = super::link_resolution::find_link_target(&root, &link)?;
        let index_parchment = parchment_base.join("index.md")?;

        let parchment_state = (
            parchment_base,
            if index_parchment.exists()? {
                Some(index_parchment)
            } else {
                None
            },
        );
        Ok(parchment_state)
    }
}
impl From<::vfs::VfsError> for ParchmentError {
    fn from(vfs_error: ::vfs::VfsError) -> Self {
        ParchmentError::IoError(format!("{}", vfs_error))
    }
}

impl ParchmentRepository for VfsParchmentRepository {
    fn resolve_link(&self, link: &Link) -> super::Result<OrdSet<Link>> {
        event!(Level::TRACE, "resolve_link: {}", link);
        let Link(PackageManager(pm), _) = link;
        if pm != super::PARCHMENT_PM {
            return Ok(ordset![]);
        }
        let (_, maybe_index) = self.check_parchment_state(link)?;

        let parchment_state = maybe_index;
        match parchment_state {
            Some(index_parchment) => {
                let parchment_links = slurp_parchment(index_parchment)?;
                Ok(parchment_links)
            }
            None => Ok(OrdSet::default()),
        }
    }
}

fn slurp_parchment(parchment: VfsPath) -> super::Result<OrdSet<Link>> {
    event!(Level::TRACE, "slurp parchment: {}", parchment.as_str());
    Ok(crate::parchment::pulldown::extract_links(
        &parchment.read_to_string()?,
    ))
}

#[cfg(test)]
macro_rules! setup_files {
        ($($($dir:ident/)* $file:literal : $contents:literal)*)=> {{
                let dotfiles: ::vfs::VfsPath = ::vfs::MemoryFS::new().into();
                $(
                  let pwd = &dotfiles;
                  $(
                    let pwd = pwd.join(stringify!($dir))?;
               	    pwd.create_dir()?;
                  )*
               	  pwd
               	  .join($file)?
               	  .create_file()?
               	  .write_all($contents)?;
                )*
                crate::parchment::vfs::VfsParchmentRepository::new(dotfiles)
        }};
}
