use crate::model::{Host, Link};
use ::directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::fs;
use std::path::PathBuf;
use thiserror::Error;

mod directories;
mod vfs;

#[derive(Debug)]
pub enum Directories {
    Home,
    Config,
}

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error("host not found")]
    CouldNotResolveHostName(),
    #[error("could not resolve `{0:?}`")]
    CouldNotResolveDirectory(Directories),
    #[error("file not found `{0:?}`")]
    FileNotFound(String),
    #[error("IO error: {0}")]
    IOError(#[from] std::io::Error),
    #[error("TOML error: {0}")]
    TOMLError(#[from] toml::de::Error),
    #[error("Other: {0}")]
    Other(String),
}

type Result<T> = std::result::Result<T, ConfigError>;
pub struct HomeDir();
impl Display for HomeDir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "~")
    }
}

/// The directory for configuration files
#[derive(Debug)]
pub struct ConfigDir<F>(pub F);

#[derive(Debug)]
pub struct ConfigLocation<T>(pub ConfigDir<T>);

/// The directory for dotfiles
#[derive(Debug, Serialize, Deserialize)]
pub struct Dotfiles(pub String);

/// The directory for pm wrappers
#[derive(Debug)]
pub struct PMWrapperDir<T>(pub T);

impl<F> Display for ConfigDir<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<CONFIG_DIR>")
    }
}

impl Into<PathBuf> for &ConfigDir<PathBuf> {
    fn into(self) -> PathBuf {
        let ConfigDir(config_dir) = self;
        config_dir.to_owned()
    }
}

const CONFIG_NAME: &'static str = "config.toml";

impl ConfigLocation<PathBuf> {
    pub fn load(&self) -> Result<Config> {
        let ConfigLocation(ConfigDir(config_file)) = self;
        let mut config_file = config_file.clone();
        config_file.push(CONFIG_NAME);
        if config_file.exists() {
            let buffer = fs::read_to_string(config_file)?;
            let config: Config = toml::from_str(&buffer)?;
            Ok(config)
        } else {
            Ok(Config::default())
        }
    }

    pub fn exists(&self) -> bool {
        let ConfigLocation(ConfigDir(config_file)) = self;
        let mut config_file = config_file.clone();
        config_file.push(CONFIG_NAME);
        config_file.exists()
    }
}

impl Into<ConfigLocation<PathBuf>> for &ProjectDirs {
    fn into(self) -> ConfigLocation<PathBuf> {
        ConfigLocation(self.into())
    }
}

impl<T> Display for ConfigLocation<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<config>/{}", CONFIG_NAME)
    }
}

impl Host {
    /// creates an instance with the name from the current host
    pub fn this() -> Result<Host> {
        let hostname = hostname::get().map_err(|_| ConfigError::CouldNotResolveHostName())?;
        Ok(Host(hostname))
    }
}

impl Default for Host {
    fn default() -> Self {
        Self::this().expect("can lookup host")
    }
}

impl Default for Dotfiles {
    fn default() -> Self {
        Self("dotfiles".to_owned())
    }
}

impl Display for Dotfiles {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(fmt, "~/{}", self.0)
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct Config {
    pub dotfiles: Dotfiles,
}

impl Default for Config {
    fn default() -> Self {
        let dotfiles = Dotfiles::default();
        Self { dotfiles }
    }
}

impl Display for Config {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(fmt, "dotfiles: {}", self.dotfiles)
    }
}

/// Lookup the parchment path for this host
pub fn host_parchment_link(host: &Host) -> Result<Link> {
    Ok(Link::new(format!("parchment_host_{}", host)))
}

#[cfg(test)]
mod test {
    use super::Host;
    use crate::config::{Config, Dotfiles};
    use directories::BaseDirs;
    use vfs::{PhysicalFS, VfsPath};
    #[test]
    fn this_host_is_this_host() {
        let this_host = hostname::get().unwrap();
        assert_eq!(Host::this().unwrap(), Host(this_host));
    }

    #[test]
    fn lookup_dotfiles_root() {
        let base_dirs = BaseDirs::new().unwrap();
        let config = Config {
            dotfiles: Dotfiles::default(),
        };
        let dotfiles_root = config.lookup_dotfiles_root().unwrap();
        let expected_path: VfsPath = PhysicalFS::new(base_dirs.home_dir().to_owned()).into();
        let expected_path = expected_path.join("dotfiles").unwrap();
        assert_eq!(dotfiles_root.as_str(), expected_path.as_str());
    }
}
