use super::{Config, ConfigError, Directories, Dotfiles, Result};
use ::vfs::{PhysicalFS, VfsError, VfsPath};
use directories::BaseDirs;

impl From<VfsError> for ConfigError {
    fn from(e: VfsError) -> Self {
        (&e).into()
    }
}

impl From<&VfsError> for ConfigError {
    fn from(e: &VfsError) -> Self {
        match e {
            VfsError::FileNotFound { path } => ConfigError::FileNotFound(path.to_owned()),
            VfsError::IoError(io_error) => ConfigError::IOError(io_error.kind().into()),
            VfsError::InvalidPath { path } => ConfigError::Other(format!("invalid path: {}", path)),
            VfsError::NotSupported => ConfigError::Other("Not supported".to_string()),
            VfsError::Other { message } => ConfigError::Other(message.to_owned()),
            VfsError::WithContext { cause, .. } => cause.as_ref().into(),
        }
    }
}

impl Config {
    /// Lookup the dotfiles root
    pub fn lookup_dotfiles_root(&self) -> Result<VfsPath> {
        let &Config {
            dotfiles: Dotfiles(name),
            ..
        } = &self;
        if let Some(base_dirs) = BaseDirs::new() {
            let h: VfsPath = PhysicalFS::new(base_dirs.home_dir().to_owned()).into();
            h.join(&name).map_err(|e| e.into())
        } else {
            Err(ConfigError::CouldNotResolveDirectory(Directories::Home))
        }
    }
}
