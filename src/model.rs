use im::{ordset, OrdMap, OrdSet};
use std::ffi::OsString;
use std::fmt::Display;

/// A host name
#[derive(Debug, PartialEq)]
pub struct Host(pub OsString);
impl Display for Host {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(fmt, "{}", self.0.to_string_lossy())?;
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub struct PackageManager(pub String);
impl Display for PackageManager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{}>", &self.0)
    }
}

pub type Package = String;
#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub struct Link(pub PackageManager, pub Package);
impl Display for Link {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Link(pm, pkg) => write!(fmt, "{}:{}", pm.0, pkg),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub struct Dependency {
    pub from: Link,
    pub to: Link,
}
impl Display for Dependency {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let Dependency { from, to } = self;
        write!(fmt, "({}->{})", from, to)?;
        Ok(())
    }
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub enum Status {
    Installed,
    NotInstalled,
    MissingPm(PackageManager),
    Unknown(String),
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, Clone)]
pub struct LinkStatus(pub Link, pub Status);

pub const PARCHMENT_PM: &str = "parchment";
impl Link {
    pub(crate) fn new<S: ToString>(s: S) -> Link {
        Link(PackageManager(PARCHMENT_PM.to_owned()), s.to_string())
    }

    pub(crate) fn package_link<PS: ToString, S: ToString>(package_manager: PS, package: S) -> Link {
        Link(
            PackageManager(package_manager.to_string()),
            package.to_string(),
        )
    }

    pub(crate) fn depends_on(self, other: Link) -> Dependency {
        Dependency::new(self, other)
    }
}

impl Dependency {
    fn new(from: Link, to: Link) -> Dependency {
        Dependency { from, to }
    }
}

impl From<&str> for Status {
    fn from(s: &str) -> Self {
        match s {
            "INSTALLED" => Status::Installed,
            "NOT_INSTALLED" => Status::NotInstalled,
            unknown_status => Status::Unknown(unknown_status.to_owned()),
        }
    }
}

pub trait LinkOperations {
    fn links_by_package_manager(&mut self) -> OrdMap<PackageManager, OrdSet<Link>>;
}

impl<'a, T> LinkOperations for T
where
    T: Iterator<Item = &'a Link>,
{
    fn links_by_package_manager(&mut self) -> OrdMap<PackageManager, OrdSet<Link>> {
        self.fold(
            OrdMap::new(),
            |acc: OrdMap<PackageManager, OrdSet<Link>>, link| {
                let Link(package_manager, _) = &link;
                acc.update_with(
                    package_manager.clone(),
                    ordset![link.to_owned()],
                    |ov, nv| ov.union(nv),
                )
            },
        )
    }
}
